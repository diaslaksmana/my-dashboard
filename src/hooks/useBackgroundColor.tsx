import { useState, useEffect } from 'react';

const useBackgroundColor = (): [string, (color: string) => void] => {
  const [backgroundColor, setBackgroundColor] = useState<string>('');

  useEffect(() => {
    const savedColor = localStorage.getItem('backgroundColor');
    if (savedColor) {
      setBackgroundColor(savedColor);
    }
  }, []);

  const setColor = (color: string) => {
    setBackgroundColor(color);
    localStorage.setItem('backgroundColor', color);
  };

  return [backgroundColor, setColor];
};

export default useBackgroundColor;
