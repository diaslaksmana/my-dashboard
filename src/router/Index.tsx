import { Route,Routes } from "react-router-dom";
import ComingSoon from "../view/layout/ComingSoon";
import Error from "../view/layout/error/Error";
import Error401 from "../view/layout/error/Error401";
import Error502 from "../view/layout/error/Error502";
import Backlog from "../view/page/Backlog";
import Contact from "../view/page/Contact";
import Dashboard from "../view/page/Dashboard";
import Home from "../view/page/Home";
import Issue from "../view/page/Issue";
import Login from "../view/page/Login";
import Preferences from "../view/page/Preferences";
import Project from "../view/page/Project";
import Schedule from "../view/page/Schedule";
import Timeline from "../view/page/Timeline";

const Router = () => {
    return (
      <Routes >
          <Route  path="/" element={<Login />}/>   
          <Route path="/dashboard" element={<Dashboard />}>
            <Route path="/dashboard"  element={<Home />}/>
            <Route path="home"  element={<Home />}/>
            <Route  path="schedule" element={<Schedule />}/>  
            <Route  path="timeline" element={<Timeline />}/> 
            <Route  path="backlog" element={<Backlog />}/> 
            <Route  path="issue" element={<Issue />}/> 
            <Route  path="contact" element={<Contact />}/> 
            <Route  path="project" element={<Project />}/> 
            <Route  path="preferences" element={<Preferences />}/>
          </Route>
          <Route  path="/error" element={<Error />}/>   
          <Route  path="/error401" element={<Error401 />}/>   
          <Route  path="/comingsoon" element={<ComingSoon />}/>   
          <Route  path="/error502" element={<Error502 />}/>   
      </Routes>
    )
  }
  export default Router;