import * as React from "react"

function Notif(props:any) {
  return (
    <svg
      data-name="Design Convert"
      id="Design_Convert"
      viewBox="0 0 64 64"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <defs>
        <style>{".cls-1{fill:#010101}"}</style>
      </defs>
      <path
        className="cls-1"
        d="M32 57a7 7 0 01-7-7 1 1 0 011-1h12a1 1 0 011 1 7 7 0 01-7 7zm-4.9-6a5 5 0 009.8 0z"
      />
      <path
        className="cls-1"
        d="M51 51H13a1 1 0 01-1-1v-8a1 1 0 01.22-.63A17.32 17.32 0 0016 30.59V27a16 16 0 0111-15.19 5 5 0 0110 0A16 16 0 0148 27v3.6a17.32 17.32 0 003.78 10.78A1 1 0 0152 42v8a1 1 0 01-1 1zm-37-2h36v-6.66a19.33 19.33 0 01-4-11.75V27a14 14 0 00-10.32-13.49 1 1 0 01-.73-1.1A2.93 2.93 0 0035 12a3 3 0 00-6 0v.32a1.1 1.1 0 01-.71 1.19A14 14 0 0018 27v3.6a19.33 19.33 0 01-4 11.75z"
      />
      <path
        className="cls-1"
        d="M41.68 26a1 1 0 01-1-.76A9.21 9.21 0 0032 18a1 1 0 010-2 11.2 11.2 0 0110.65 8.76 1 1 0 01-1 1.24z"
      />
    </svg>
  )
}

export default Notif
