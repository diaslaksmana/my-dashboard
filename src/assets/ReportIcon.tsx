import * as React from "react"

function ReportIcon(props:any) {
  return (
    <svg className="reportIcon" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" {...props}>
      <defs>
        <style>
          {
            ".cls-1{fill:none;stroke:#000;stroke-linejoin:round;stroke-width:2px}"
          }
        </style>
      </defs>
      <g data-name="329-Document Report" id="_329-Document_Report">
        <path className="cls-1" d="M10 1L4 7 4 31 28 31 28 1 10 1z" />
        <path className="cls-1" d="M10 1L10 7 4 7" />
        <path className="cls-1" d="M9 12L9 26 24 26" />
        <path className="cls-1" d="M9 26L16 19 19 22 24 17" />
      </g>
    </svg>
  )
}

export default ReportIcon
