import * as React from "react"

function HumbergerIcon(props:any) {
  return (
    <svg
      data-name="Layer 1"
      id="Layer_1"
      viewBox="0 0 48 48"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <defs>
        <style>
          {
            ".cls-1{fill:none;stroke:#231f20;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}"
          }
        </style>
      </defs>
      <path className="cls-1  cls1" d="M1 10L47 10" />
      <path className="cls-1 cls2" d="M1 24L47 24" />
      <path className="cls-1 cls3" d="M1 38L47 38" />
    </svg>
  )
}

export default HumbergerIcon
