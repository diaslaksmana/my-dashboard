import * as React from "react"

function Logout(props:any) {
  return (
    <svg
      data-name="Layer 2"
      viewBox="0 0 35 35"
      xmlns="http://www.w3.org/2000/svg"
      className="logout"
      {...props}
    >
      <path d="M17.54 34.75a17.25 17.25 0 010-34.5 1.25 1.25 0 010 2.5 14.75 14.75 0 000 29.5 1.25 1.25 0 010 2.5z" />
      <path d="M32.927 18.75H15.25a1.25 1.25 0 010-2.5h17.677a1.25 1.25 0 010 2.5z" />
      <path d="M26.536 26.438a1.25 1.25 0 01-.884-2.134l6.384-6.385a.6.6 0 000-.839l-6.384-6.38a1.25 1.25 0 111.768-1.771l6.38 6.384a3.1 3.1 0 010 4.374l-6.38 6.385a1.246 1.246 0 01-.884.366z" />
    </svg>
  )
}

export default Logout
