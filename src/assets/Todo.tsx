import * as React from "react"

function Todo(props:any) {
  return (
    <svg
      id="Icons"
      viewBox="0 0 32 32"
      xmlSpace="preserve"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <style>
        {
          ".st0{fill:none;stroke:#000;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10}"
        }
      </style>
      <path
        className="st0"
        d="M11 13H5c-1.1 0-2-.9-2-2V5c0-1.1.9-2 2-2h6c1.1 0 2 .9 2 2v6c0 1.1-.9 2-2 2zM11 29H5c-1.1 0-2-.9-2-2v-6c0-1.1.9-2 2-2h6c1.1 0 2 .9 2 2v6c0 1.1-.9 2-2 2z"
      />
      <path className="st0" d="M17 5L29 5" />
      <path className="st0" d="M17 9L24 9" />
      <path className="st0" d="M17 21L29 21" />
      <path className="st0" d="M17 25L24 25" />
      <path className="st0" d="M6 7L8 9 11 6" />
    </svg>
  )
}

export default Todo
