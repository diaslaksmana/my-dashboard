import React from 'react'
import { NavLink } from 'react-router-dom'
import ErrorImg from '../../../assets/png/401.png'
export const Error401 = () => {
  return (
    <div className='errorPage flex flex-col items-center justify-center h-[100vh]'>
        <img src={ErrorImg} alt="error"/>
        <h1 className='text-3xl mb-10 mt-6'>Oooppss.... something wrong error 401 </h1>
        <NavLink to='/dashboard/home'>Back to Home</NavLink>
    </div>
  )
}
export default Error401
