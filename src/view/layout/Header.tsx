import { useEffect, useState } from 'react'
import HumbergerIcon from '../../assets/Humberger';
import Notif from '../../assets/Notif';
import RightIcon from '../../assets/RightIcon';
import Brand from '../../assets/png/brand.png'
import Close from '../../assets/Close';
export const Header = () => {
  const [isActive, setIsActive] = useState<boolean>(false);
  const [scrollClass, setScrollClass] = useState<string>('');
  const [isVisible, setIsVisible] = useState<boolean>(false);
  const [modeSidebar, setModeSidebar] = useState<string>('');


  //Setting sidebar Mode
  useEffect(() => {
    const savedTheme = localStorage.getItem('modeSidebar');
      if (savedTheme) {
        setModeSidebar(savedTheme);
      } else {
          const prefermodeSidebar = window.matchMedia('(prefers-mode-scheme: showSidebar)').matches;
          setModeSidebar(prefermodeSidebar ? 'showSidebar' : 'hideSidebar');
      }
  }, []);

    const handlemodeSidebar = (modeSidebar: string) => {
      setModeSidebar(modeSidebar);
        localStorage.setItem('modeSidebar', modeSidebar);
    };

    useEffect(() => {
        document.documentElement.setAttribute('data-theme-sidebarmode', modeSidebar);
    }, [modeSidebar]);

  
    //scrolled
  const toggleVisibility = () => {
    setIsVisible(!isVisible);
  };
  
  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;
      const newScrollClass = scrollPosition > 0 ? 'scroll' : '';
      setScrollClass(newScrollClass);
    };

    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);


  //togle
  const toggleActiveClass = () => {
    setIsActive(!isActive);
  };

  useEffect(() => {
    if (isActive) {
      document.body.classList.add('active__menu');
    } else {
      document.body.classList.remove('active__menu');
    }
  }, [isActive]);
  return (
    <header id='header' className={`header ${scrollClass}`}>
      <nav className='container mx-auto px-5 md:px-3 py-5 md:py-3 flex items-center justify-between'>
          <div className='headerLeft'>
            <div className='headerLeft--list '>
              <ul className='flex items-center gap-2'>
                <li className='headerLeft--brand'>
                  <button> <img src={Brand} alt='brand'/> </button>
                </li>
                <li className='headerLeft--humberger'>
                  <button onClick={() => handlemodeSidebar('hideSidebar')} className='showSidebar'> <RightIcon/> </button>
                  <button onClick={() => handlemodeSidebar('showSidebar')} className='hideSidebar'> <HumbergerIcon/> </button>
                </li>
              </ul>
              
            </div>
            
          </div>       
          <div className='headerRight'>
            <ul className='headerRight--menu flex items-baseline justify-center gap-4'>
              <li className={` dropdown--notif ${isVisible ? 'show' : ''}`}>
               
                <button onClick={toggleVisibility} className="flex relative"> <Notif/> <span className='notif--label'>2</span></button>
              </li>
              <li>
                <button className='humberger' onClick={toggleActiveClass}> 
                  {isActive ? <Close/> : <HumbergerIcon/> }
                </button>
              </li>
            </ul>
            {isVisible &&
              <div className='panel--notif'>
                <h1 className='text-lg border-b pl-2'>Notification  </h1>
                <ul className='list--notif'>
                  <li>
                    Lorem ipsum dolor sit amet consectetur adipisicing 
                  </li>
                  <li>
                    Lorem ipsum dolor sit amet  
                  </li>
                  <li>
                    Lorem ipsum 
                  </li>
                  <li>
                    Lorem ipsum dolor sit amet consectetur  
                  </li>
                </ul>
              </div>
            }
          </div>  
          
      </nav>
    </header>
  )
}

export default Header