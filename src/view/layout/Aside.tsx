import { useEffect, useRef, useState } from 'react'
import { NavLink } from 'react-router-dom'
import Contact from '../../assets/Contact'
import Dash from '../../assets/Dash'
import Issue from '../../assets/Issue'
import Logout from '../../assets/logout'
import Plan from '../../assets/Plan'
import Preferences from '../../assets/Preferences'
import Project from '../../assets/Project'
import Task from '../../assets/Task'
import Team from '../../assets/Team'
import Time from '../../assets/Time'
import Avatar from '../../assets/avatar.png'
import Page from '../../assets/Page'
import Down from '../../assets/Down'
import Brand from '../../assets/png/brand.png'
import AddTeam from '../../assets/AddTeam'

export const Aside = () => {
    const [isVisible, setIsVisible] = useState<boolean>(false);
    const [isVisiblePage, setIsVisiblePage] = useState<boolean>(false);
    const toggleVisibility = () => {
      setIsVisible(!isVisible);
    };
    const toggleVisibilityPage = () => {
        setIsVisiblePage(!isVisiblePage);
    };

  

  return (
    <aside className='aside'>
         <div className='container mx-auto px-5 md:px-3 py-5 md:py-3 aside__Item'>
           <div className='aside__Item--top'>
                <div className='brand'>
                    <img src={Brand} alt='brand'/> <h1 className='font-bold text-2xl'> Pine App</h1>
                </div>
                <div className='navbar '>
                    <h1 className='font-semibold text-base'>My Desk</h1>
                    <ul className='mb-5 navbar__List'>
                        <li>
                           <NavLink to='/dashboard/home'> <Dash/> Dashboard</NavLink>
                        </li>
                        <li>
                            <NavLink to='/dashboard/schedule'> <Time/> My Shcedule</NavLink>
                        </li>
                        <li className={` dropdown ${isVisiblePage ? 'show' : ''}`}>
                            <button onClick={toggleVisibilityPage} ><span className='btn--icon'><Page/> Page</span> <span className='btn--iconDown'><Down/> </span> </button>
                            {isVisiblePage &&
                             <ul className='sub--navbar__List list-disc pl-[25px]'>
                                <li> <NavLink to='/error'>Error 404</NavLink></li>
                                <li> <NavLink to='/error401'>Error 401</NavLink></li>
                                <li> <NavLink to='/error502'>Bad Gatway</NavLink></li>
                                <li> <NavLink to='/comingsoon'>Coming Soon</NavLink></li>
                                <li> <NavLink to='/'>Login</NavLink></li>
                            </ul>
                            }
                           
                        </li>
                    </ul>
                    <h1 className='font-semibold text-base'>Board</h1>
                    <ul className='mb-5 navbar__List'>
                        <li>
                           <NavLink to='/dashboard/timeline'><Plan/>Timeline</NavLink>
                        </li>
                        <li>
                            <NavLink to='/dashboard/backlog'><Task/>Backlog</NavLink>
                        </li>
                        <li>
                            <NavLink to='/dashboard/project'><Project/> Project</NavLink>
                        </li>
                        <li>
                            <NavLink to='/dashboard/issue'><Issue/> Issue</NavLink>
                        </li>
                        <li className={` dropdown ${isVisible ? 'show' : ''}`}>
                            <button onClick={toggleVisibility} ><span className='btn--icon'><Team/> Team</span> <span className='btn--iconDown'><Down/> </span> </button>
                            {isVisible &&
                             <ul className='sub--navbar__List'>
                                <li> <NavLink to='/error'><AddTeam/>Member</NavLink></li>
                                <li> <NavLink to='/error'><AddTeam/>Add Member</NavLink></li>
                            </ul>
                            }
                           
                        </li>
                    </ul>
                    <h1 className='font-semibold text-base'>Setting</h1>
                    <ul className='mb-5 navbar__List aside__setting'>
                        <li>
                           <NavLink to='/dashboard/preferences'><Preferences/> Preferences</NavLink>
                        </li>
                        <li>
                           <NavLink to='/dashboard/contact'><Contact/> Contact</NavLink>
                        </li>
                    </ul>
                </div>
           </div>
            <div className='aside__Item--bottom'>
                <div className='account relative'>
                    <NavLink to='/' className='absolute h-full w-full z-10'> </NavLink>
                        <div className='flex gap-2'>
                            <img src={Avatar} alt='avatar'/> 
                            <div className=''>
                                <h1 className='font-semibold '> John Dae </h1>
                                <p className='text-xs'>Frontend Developer</p>
                            </div>
                        </div>
                        <Logout/> 
                </div>
            </div>
         </div>
    </aside>
  )
}

export default Aside
