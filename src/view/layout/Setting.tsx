import { useEffect, useState } from 'react'
import SettingIcon from '../../assets/Setting'

export const Setting = () => {
    const [isActive, setIsActive] = useState<boolean>(false);
    const [backgroundClass, setBackgroundClass] = useState<string>('');
    const [theme, setTheme] = useState<string>('');
    const [themeBg, setThemeBg] = useState<string>('');
    const [themeColor, setThemeColor] = useState<string>('');

    //setting dark mode
    useEffect(() => {
        const savedTheme = localStorage.getItem('theme');
        if (savedTheme) {
            setTheme(savedTheme);
        } else {
            const prefersDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
            setTheme(prefersDarkMode ? 'Dark' : 'Light');
        }
    }, []);
    
    const toggleTheme = (newTheme: string) => {
        setTheme(newTheme);
        localStorage.setItem('theme', newTheme);
    };
    
    useEffect(() => {
        document.documentElement.setAttribute('data-theme', theme);
    }, [theme]);
  

    //open panel setting
    const toggleActiveClass = () => {
        setIsActive(!isActive);
    };
    useEffect(() => {
        if (isActive) {
        document.body.classList.add('setting__panel');
        } else {
        document.body.classList.remove('setting__panel');
        }
    }, [isActive]);

    //theme color
    useEffect(() => {
        // Load the background class from storage
        const storedBackgroundClass = localStorage.getItem('backgroundClass');
        if (storedBackgroundClass) {
          setBackgroundClass(storedBackgroundClass);
        }
      }, []);
    
    const handleChangeColor = (newClass: string) => {
        setBackgroundClass(newClass);
        // Save the background class to storage
        localStorage.setItem('backgroundClass', newClass);
    };
    useEffect(() => {
        document.documentElement.setAttribute('data-theme-sidebar', backgroundClass);
    }, [backgroundClass]);

    //theme bg sidebar
    useEffect(() => {
        // Load the background class from storage
        const storedBackgroundImg = localStorage.getItem('themeBg');
        if (storedBackgroundImg) {
            setThemeBg(storedBackgroundImg);
        }
      }, []);
    
    const handleChangeBg = (newClassImg: string) => {
        setThemeBg(newClassImg);
        // Save the background class to storage
        localStorage.setItem('themeBg', newClassImg);
    };
    useEffect(() => {
        document.documentElement.setAttribute('data-theme-sidebar-bg', themeBg);
    }, [themeBg]);


     //theme Theme Color
     useEffect(() => {
        // Load the background class from storage
        const storedThemeColor = localStorage.getItem('themeColor');
        if (storedThemeColor) {
            setThemeColor(storedThemeColor);
        }
      }, []);
    
    const handleChangeThemeColor = (newClassThemeColor: string) => {
        setThemeColor(newClassThemeColor);
        // Save the background class to storage
        localStorage.setItem('themeColor', newClassThemeColor);
    };
    useEffect(() => {
        document.documentElement.setAttribute('data-theme-color', themeColor);
    }, [themeColor]);

  return (
    <div id='setting'>
        <button className='btn__setting' onClick={toggleActiveClass}> <SettingIcon/> </button>

        <div className='setting__panel--show'>
            <div className='content__setting container mx-auto px-5 md:px-3 py-5 md:py-5'>
                <div className='flex justify-between items-center border-b pb-3'>
                    <h1 className='text-xl font-bold'>Theme Customizer</h1>
                    <button className='font-bold text-xl' onClick={toggleActiveClass}> X </button>
                </div>
                <div className='theme my-5'>
                    <h1 className='capitalize'>Option Mode: <span className='font-bold'> {`${theme}`}</span> </h1>
                    <div className='flex items-center gap-3'>
                        <button onClick={() => toggleTheme('dark')} className='btn__dark' >Dark</button>
                        <button onClick={() => toggleTheme('light')}className='btn__light' >Light</button>
                    </div>
                    
                </div>
                
                <div className='theme--colorSidebar my-5'>
                    <h1 className='capitalize'>Sidebar Color: <span className='font-bold'>{` ${backgroundClass}`}</span> </h1>
                   <ul>
                        <li>
                            <button onClick={() => handleChangeColor('transparent')} className='transparent'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeColor('black')} className='black'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeColor('purple')} className='purple'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeColor('gradient')} className='gradient'></button>
                        </li>
                   </ul>
                </div>
                
                <div className='theme--bg'>
                    <h1 className='capitalize'>Background Sidebar: <span className='font-bold'>{` ${themeBg}`}</span> </h1>
                    <ul className='grid grid-cols-3 gap-4'>
                        <li>
                            <button onClick={() => handleChangeBg('no')} className='no__image'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeBg('beach1')} className='beach1'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeBg('beach2')} className='beach2'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeBg('building')} className='building'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeBg('office')} className='office'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeBg('sky')} className='sky'></button>
                        </li>
                   </ul>
                </div>

                <div className='theme--color my-5'>
                    <h1 className='capitalize'>Theme Color: <span className='font-bold'>{` ${themeColor}`}</span> </h1>
                    <ul>
                        <li>
                            <button onClick={() => handleChangeThemeColor('primary')} className='theme--primary'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeThemeColor('blue')} className='theme--blue'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeThemeColor('green')} className='theme--green'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeThemeColor('orange')} className='theme--orange'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeThemeColor('purple')} className='theme--purple'></button>
                        </li>
                        <li>
                            <button onClick={() => handleChangeThemeColor('aqua')} className='theme--aqua'></button>
                        </li>
                   </ul>
                </div>

            </div>           
        </div>
    </div>
  )
}

export default Setting