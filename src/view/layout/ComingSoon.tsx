import React from 'react'
import { NavLink } from 'react-router-dom'
import ErrorImg from '../../assets/png/soon.png'

export const ComingSoon = () => {
  return (
    <div className='errorPage flex flex-col items-center justify-center h-[100vh]'>
        <img src={ErrorImg} alt='comingsoon'/>
        <p className='my-8 text-center'>
            Welcome to our website! We're thrilled to announce that something amazing is coming soon. <br/> Our team has been hard at work crafting an exceptional online experience for you, and we can't wait to unveil it
        </p>
        <NavLink to='/dashboard/home'>Back to Home</NavLink>
    </div>
  )
}

export default ComingSoon