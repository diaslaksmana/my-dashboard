import React, { useState } from 'react';
import Down from '../../assets/Down';
import UpIcon from '../../assets/UpIcon';

interface AccordionProps {
  title: string;
  children: React.ReactNode;
}

const Accordion: React.FC<AccordionProps> = ({ title, children }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleAccordion = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <button onClick={toggleAccordion}>
        {title} {isOpen ? <Down/> : <UpIcon/>}
      </button>
      {isOpen && <>{children}</>}
    </>
  );
};

export default Accordion;
