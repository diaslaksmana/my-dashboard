import React from 'react'
import ComingSoon from '../layout/ComingSoon'

export const Issue = () => {
  return (
    <section id='issue' >  
         <div className='issue container  mx-auto px-5 md:px-3 py-5 md:py-7'>
            <ComingSoon/>
         </div>
    </section>
  )
}

export default Issue