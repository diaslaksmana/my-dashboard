import { Outlet } from 'react-router-dom'
import Aside from '../layout/Aside'
import Footer from '../layout/Footer'
import Header from '../layout/Header'
import Setting from '../layout/Setting'

export const Dashboard = () => {
  return (
    <div id='dashboard' className='block sm:flex  content-center gap-5'>
        <Aside/>   
      <div className='section--content'>
          <Header/>    
          <Outlet />
          <Setting/>
          <Footer/>
      </div>
     
    </div>
  )
}

export default Dashboard