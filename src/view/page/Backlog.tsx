import React from 'react'
import { NavLink } from 'react-router-dom'
import Up from '../../assets/Up'
import Avatar from '../../assets/avatar.png'
import Medium from '../../assets/Medium'
import DownIcon from '../../assets/DownIcon'

export const Backlog = () => {
  return (
    <section id='backlog' >  
         <div className='backlog container  mx-auto px-5 md:px-3 py-5 md:py-7'>
            <div className='backlog--header mb-4 flex justify-between'>
                <h1 className='text-3xl'>Backlog</h1>     
                <button className='btn--addTask'>Add Task</button>
            </div>         
            <div className='backlogBox'>
                <div className='backlogBox--list'>
                    <h1 className='backlogHeading '>Sprint 10.1  <span className='text-xs'>15 Task</span></h1>
                    <p className='text-xs mb-2'>21/jul/2023</p>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='hight'><Up/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-123</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='medium'><Medium/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-124</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='low'><DownIcon/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-125</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='hight'><Up/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-123</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='medium'><Medium/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-124</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='low'><DownIcon/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-125</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='hight'><Up/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-123</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='medium'><Medium/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-124</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='low'><DownIcon/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-125</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='hight'><Up/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-123</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='medium'><Medium/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-124</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='low'><DownIcon/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-125</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>

                </div>

                <div className='backlogBox--list'>
                    <h1 className='backlogHeading '>Backlog  <span className='text-xs'>15 Task</span></h1>
                    <p className='text-xs mb-2'>21/jul/2023</p>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='hight'><Up/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-123</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='medium'><Medium/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-124</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='low'><DownIcon/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-125</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='hight'><Up/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-123</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='medium'><Medium/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-124</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='low'><DownIcon/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-125</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='hight'><Up/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-123</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='medium'><Medium/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-124</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='low'><DownIcon/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-125</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='hight'><Up/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-123</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='medium'><Medium/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-124</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>
                    <NavLink to='project'>
                        <div className='backlogBox--listItem flex justify-between items-center'>                            
                            <div className='backlogBox--listTitle'>
                                <h1 className='flex items-center gap-1 text-sm'><label className='low'><DownIcon/> </label>Lorem ipsum dolor sit amet consectetur adipisicing elit.  </h1>
                            </div>
                            <div className='backlogBox--listStatus py-1'>
                                <label className='text-xs'>P-125</label>
                                <img src={Avatar} alt='avatar'/> 
                            </div>                       
                        </div>
                    </NavLink>

                </div>



            </div>
         </div>
    </section>
  )
}

export default Backlog