import Hands from '../../assets/Hands';
import Issue from '../../assets/Issue';
import PendingIcon from '../../assets/PendngIcon';
import ProductIcon from '../../assets/ProductIcon';
import Human from '../../assets/human.png'
import Project1 from '../../assets/png/project1.png'
import Project2 from '../../assets/png/project2.png'
import Project3 from '../../assets/png/project3.png'
import Project4 from '../../assets/png/project4.png'
import { NavLink } from 'react-router-dom';
import ChartBar from '../chart/ChartBar';
import Discus from '../../assets/png/discus.png'
import AreaChart from '../chart/AreaChart';
import ColumnChart from '../chart/ColumnChart';
import { useEffect, useState } from 'react';

export const Home = () => {
  const [currentTime, setCurrentTime] = useState(new Date());
  const day = currentTime.toLocaleDateString(undefined, { weekday: "long" });
  useEffect(() => {
    const timer = setInterval(() => {
      setCurrentTime(new Date());
    }, 1000);

    return () => {
      clearInterval(timer);
    };
  }, []);
  const dateTime = currentTime.toLocaleString(undefined, {
    day: "numeric",
    month: "long",
    year: "numeric",
  });

  return (
    <section id='home'>  
      <div className='home container  mx-auto px-5 md:px-3 py-5 md:py-7'>
          <div className='home__title'>
              <div  className='home__title--left'>
                <h1 className='text-3xl'>Hi, John Dae <Hands/> </h1>
                <p>Welcome to My App dashboard </p>
              </div>
              <div  className='home__title--right'>
                <div className="current-date">
                  <div className="day text-base"> <h1>{day} /</h1> </div>
                  <div className="datetime text-sm"><p>{dateTime}</p></div>
                </div>
              </div>             
          </div>

          {/* Info Box */}
          
          <div className='info grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5  gap-4 mt-10'>
            
            <div className='info__box info__box--custom col-span-1 md:col-span-1 lg:col-span-2 flex flex-col justify-center'>
              <div className='info__box--title flex items-center  gap-4'>
                <p className='text-sm'>Completed Projects</p>
              </div>
              <div className='info__box--value  flex items-center justify-between'>
                  <div className='value__box flex items-center gap-2'>
                    <h1 className='font-bold text-5xl'>35</h1>
                    <span className='up'>1.5%</span>
                  </div>
              </div>                 
              <img src={Human} className='img--card' alt='human'/>          
            </div>

            <div className='info__box'>
              <div className='info__box--title flex items-center  gap-4'>
                <div className="icon">
                  <Issue/>
                </div>                
                <p className='text-sm'>Issue Report</p>
              </div>
              <div className='info__box--value  flex items-center justify-between'>
                  <div className='value__box'>
                    <h1 className='font-bold text-5xl'>5</h1>
                    <p className='text-xs'>this month</p>
                  </div>
                  <span className='down'>4.5%</span>
              </div>             
            </div>

            <div className='info__box'>
              <div className='info__box--title flex items-center  gap-4'>
                <div className="icon">
                  <ProductIcon/>
                </div>                
                <p className='text-sm'>Total Projects</p>
              </div>
              <div className='info__box--value  flex items-center justify-between'>
                  <div className='value__box'>
                    <h1 className='font-bold text-5xl'>350</h1>
                    <p className='text-xs'>since 2020</p>
                  </div>
                  <span className='up'>8.5%</span>
              </div>             
            </div>

            <div className='info__box'>
              <div className='info__box--title flex items-center  gap-4'>
                <div className="icon">
                  <PendingIcon/>
                </div>                
                <p className='text-sm'>Pending Projects</p>
              </div>
              <div className='info__box--value  flex items-center justify-between'>
                  <div className='value__box'>
                    <h1 className='font-bold text-5xl'>10</h1>
                    <p className='text-xs'>this month</p>
                  </div>
                  <span className='down'>1.5%</span>
              </div>             
            </div>

          </div>

          {/* =========== Row 2 =========== */}

          <div className='info grid grid-cols-1 sm:grid-cols-1 md:grid-cols-5  gap-4 my-5'>
            <div  className='info__box col-span-3 flex justify-between flex-col'>
              <div className='summary__project'>
              <h1 className='text-xl border-b mb-2'> Projects Summary </h1>
              <table className="listProject">
                <thead>
                  <tr>
                    <th scope="col"><h1>Project name</h1></th>
                    <th scope="col"><h1>Project Status</h1></th>
                    <th scope="col"><h1>Priority</h1></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div className='listProject--name flex items-center gap-1'>
                          <img src={Project1} alt="" />
                          <h1>Dash</h1>
                      </div>
                    </td>
                    <td>
                      <div className='listProject--status gap-1'>
                          <h1>Todo</h1>
                      </div>
                    </td>
                    <td>
                      <div className='listProject--priority '>
                         <span className='high'>High</span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div className='listProject--name flex items-center gap-1'>
                          <img src={Project2} alt="" />
                          <h1>Home Property Real Estate</h1>
                      </div>
                    </td>
                    <td>
                      <div className='listProject--status gap-1'>
                          <h1>In Progress</h1>
                      </div>
                    </td>
                    <td>
                      <div className='listProject--priority '>
                         <span className='low'>Low</span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div className='listProject--name flex items-center gap-1'>
                          <img src={Project3} alt="" />
                          <h1>Healty Center</h1>
                      </div>
                    </td>
                    <td >
                      <div className='listProject--status  gap-1'>
                          <h1>In Review</h1>
                      </div>
                    </td>
                    <td>
                      <div className='listProject--priority '>
                         <span className='medium'>Medium</span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div className='listProject--name flex items-center gap-1'>
                          <img src={Project4} alt="" />
                          <h1>Leave </h1>
                      </div>
                    </td>
                    <td >
                      <div className='listProject--status  gap-1'>
                          <h1>Done</h1>
                      </div>
                    </td>
                    <td>
                      <div className='listProject--priority '>
                         <span className='high'>High</span>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              </div>
              <div className='listProject--btn my-5'>
                <NavLink to='/project'>
                  View All Project
                </NavLink>
                 
              </div>
            </div>

            <div  className='info__box col-span-2'>
              <h1 className='text-xl border-b mb-2'> Project Overview </h1>
              <ChartBar/>
            </div>

          </div>

          {/* =========== Row 3 ==============*/}
          <div className='info row3 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3  gap-4 my-5'>
            <div  className='info__box info__box--padding flex justify-between flex-col'>
              <div className='info__box--header'>
                  <img src={Discus} alt='discuss'/>
              </div>
              <div className='info__box--body border-b py-4 px-4'>
                  <p className='text-sm'>22 March, 2021</p>
                  <h1 className='text-2xl'>Internal meeting, Discuss sprint planning</h1>
              </div>
              <div className='info__box--footer py-4 px-4'>
              <p className='text-xs'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia vero magnam, fugit dolorem quo qui.</p>
              </div>
            </div>

            <div  className='info__box flex justify-between flex-col'>
              <h1 className='text-xl border-b mb-2'> Project Overview </h1>
              <p className='text-xs'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia vero magnam, fugit dolorem quo qui.</p>
              <AreaChart/>
            </div>

            <div  className='info__box  montly--earning'>
            <h1 className='text-xl border-b mb-2'> Monthly Earnings </h1>
              <p className='text-xs'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia vero magnam, fugit dolorem quo qui.</p>
              <ColumnChart/>
            </div>

          </div>

      </div>
    </section>
  )
}

export default Home