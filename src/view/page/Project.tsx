import React from 'react'
import ComingSoon from '../layout/ComingSoon'

export const Project = () => {
  return (
    <section id='project' >  
         <div className='schedule container  mx-auto px-5 md:px-3 py-5 md:py-7'>
           <ComingSoon/>           
         </div>
    </section>
  )
}

export default Project