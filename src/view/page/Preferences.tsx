import ComingSoon from "../layout/ComingSoon"

export const Preferences = () => {
  return (
    <section id='prferences' >  
         <div className='preferences container  mx-auto px-5 md:px-3 py-5 md:py-7'>
           <ComingSoon/>
         </div>
    </section>
  )
}

export default Preferences