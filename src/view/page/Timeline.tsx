import React from 'react'

export const Timeline = () => {
  return (
    <section id='timeline' >  
        <div className='timeline container  mx-auto px-5 md:px-3 py-5 md:py-7'>
        <h1 className='text-3xl'>Timeline</h1>
            <div className='timeline--panel flex flex-row'>
                <div className='timeline--panelTitle basis-1/6'>
                    <h1 className='text-xl font-semibold'>Sprint 10</h1>
                    <p className='text-xs'>1 Aug 2023 - 31 Aug 2023</p>
                </div>
                <div className='timeline--panelList basis-5/6'>
                    <ul>
                        <li className='done'>
                            <h1>Sprint 10.0</h1>
                            <span className='text-xs'>1 Aug 2023 - 7 Aug 2023</span>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam ipsam expedita alias temporibus assumenda, sed illum cumque, hic illo laborum odit cum. Vitae, provident numquam. Assumenda quam quae veniam quidem?ta alias temporibus assumenda, sed illum cumque, hic illo laborum odit cum. Vitae, provident numquam. Assumenda quam quae veniam quidem?
                            </p>
                        </li>
                        <li className='progress'>
                            <h1>Sprint 10.1</h1>
                            <span className='text-xs'>8 Aug 2023 - 14 Aug 2023</span>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam ipsam expedita alias temporibus assumenda, sed illum cumque, hic illo laborum odit cum. Vitae, provident numquam. Assumenda quam quae veniam quidem?
                            </p>
                        </li>
                        <li className='waiting'>
                            <h1>Sprint 10.2</h1>
                            <span className='text-xs'>15 Aug 2023 - 21 Aug 2023</span>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam ipsam expedita alias temporibus assumenda, sed illum cumque, hic illo laborum odit cum. Vitae, provident numquam. Assumenda quam quae veniam quidem?
                            </p>
                        </li>
                        <li className='waiting'>
                            <h1>Sprint 10.3</h1>
                            <span className='text-xs'>22 Aug 2023 - 29 Aug 2023</span>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam ipsam expedita alias temporibus assumenda, sed illum cumque, hic illo laborum odit cum. Vitae, provident numquam. Assumenda quam quae veniam quidem?
                            </p>
                        </li>
                    </ul>
                </div>
            </div>

            <div className='timeline--panel timeline--panelDone flex flex-row'>
                <div className='timeline--panelTitle basis-1/6'>
                    <h1 className='text-xl font-semibold'>Sprint 9</h1>
                    <p className='text-xs'>1 Jul 2023 - 31 Jul 2023</p>
                </div>
                <div className='timeline--panelList basis-5/6'>
                    <ul>
                        <li className='done'>
                            <h1>Sprint 9.0</h1>
                            <span className='text-xs'>1 Jul 2023 - 7 Jul 2023</span>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam ipsam expedita alias temporibus assumenda, sed illum cumque, hic illo laborum odit cum. Vitae, provident numquam. Assumenda quam quae veniam quidem?ta alias temporibus assumenda, sed illum cumque, hic illo laborum odit cum. Vitae, provident numquam. Assumenda quam quae veniam quidem?
                            </p>
                        </li>
                        <li className='done'>
                            <h1>Sprint 9.1</h1>
                            <span className='text-xs'>8 Jul 2023 - 14 Jul 2023</span>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam ipsam expedita alias temporibus assumenda, sed illum cumque, hic illo laborum odit cum. Vitae, provident numquam. Assumenda quam quae veniam quidem?
                            </p>
                        </li>
                        <li className='done'>
                            <h1>Sprint 9.2</h1>
                            <span className='text-xs'>15 Jul 2023 - 21 Jul 2023</span>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam ipsam expedita alias temporibus assumenda, sed illum cumque, hic illo laborum odit cum. Vitae, provident numquam. Assumenda quam quae veniam quidem?
                            </p>
                        </li>
                        <li className='done'>
                            <h1>Sprint 9.3</h1>
                            <span className='text-xs'>22 Aug 2023 - 29 Aug 2023</span>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam ipsam expedita alias temporibus assumenda, sed illum cumque, hic illo laborum odit cum. Vitae, provident numquam. Assumenda quam quae veniam quidem?
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
  )
}

export default Timeline