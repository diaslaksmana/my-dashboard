import Calender from "../layout/Calender"
import DemoApp from "../layout/Calender"
import ComingSoon from "../layout/ComingSoon"

export const Schedule = () => {
 
  return (
    <section id='schedule' >  
         <div className='schedule container  mx-auto px-5 md:px-3 py-5 md:py-7'>
            <h1 className='text-3xl'>My Schedule</h1>
           <Calender/>
         </div>
    </section>
  )
}

export default Schedule