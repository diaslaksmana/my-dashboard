import LoginImg from '../../assets/png/login.png'
import Brand from '../../assets/png/brand.png'
import { NavLink } from 'react-router-dom'

export const Login = () => {
  return (
    <section id='login'className='flex flex-row '>
        <div className='loginLeft basis-3/5'>           
            <img src={LoginImg} className='img--login' alt='login'/>
            <h1 className='text-2xl'>Welcome to My App Dashboard</h1>
        </div>
        <div className='loginRight basis-full sm:basis-2/5'>
            <div className='brand'>
                <img src={Brand} alt='login'/> <h1 className='font-bold text-2xl'> Pine App</h1>
            </div>
            <div className='login--panel'>
                <h1 className='text-3xl mb-4'>Login</h1>
                <form action="">
                    <div className='form'>
                        <label htmlFor="">Username</label>
                        <input type='email' placeholder='Email'/>
                    </div>
                    <div className='form'>
                        <label htmlFor="">Password</label>
                        <input type='password' placeholder='Password'/>
                    </div>
                    <p className='btn--forgot'>Forgot Password? <NavLink to=''>click here</NavLink></p>
                    <NavLink to='/dashboard' className='btn--sginIn'>Sign In</NavLink>
                </form>
                <p className='text-sm my-4'>* just click Sign In for go to Dashboard</p>
            </div>
        </div>
    </section>
  )
}

export default Login