import React from 'react';
import ReactApexChart from 'react-apexcharts';

interface MyComponentProps {}

const ColumnChart: React.FC<MyComponentProps> = () => {
  const chartOptions = {
    // Define your chart options here
    chart: {
      height: 350,
    },
    series: [
      {
        name: 'Total Project',
        data: [30, 40, 35, 50, 49, 60, 70,30, 35, 80, 50, 49],
      },
    ],
    curve: 'smooth',
    colors: ['#164B60'],
    xaxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep','Oct','Nov','Dec'],
    },
  };

  return (
    <div>
      <ReactApexChart
        options={chartOptions}
        series={chartOptions.series}
        type="bar"
        height={250}
      />
    </div>
  );
};

export default ColumnChart;