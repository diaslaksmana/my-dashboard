import React from 'react';
import ReactApexChart from 'react-apexcharts';

interface MyComponentProps {}

const AreaChart: React.FC<MyComponentProps> = () => {
  const chartOptions = {
    // Define your chart options here
    chart: {
      height: 350,
    },
    series: [
      {
        name: 'Total Project',
        data: [30, 40, 35, 50, 49, 60, 70],
      },
    ],
    curve: 'smooth',
    colors: ['#164B60'],
    xaxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
    },
  };

  return (
    <div>
      <ReactApexChart
        options={chartOptions}
        series={chartOptions.series}
        type="area"
        height={250}
      />
    </div>
  );
};

export default AreaChart;